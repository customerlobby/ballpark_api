require 'faraday_middleware'
Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}

module BallparkApi
  module Connection
    private

    def connection
      options = {
        :url => "http://#{api_site}"
      }

      Faraday::Connection.new(options) do |connection|
        connection.use FaradayMiddleware::BallparkApiAuth, api_key
        connection.use FaradayMiddleware::Mashify
        connection.use Faraday::Response::ParseJson
        connection.adapter(adapter)
      end
    end

    def api_site
      "#{api_appid}.ballparkapp.com"
    end

  end
end
