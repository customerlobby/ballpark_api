require 'hashie'
module BallparkApi
  # Wrapper for the BallparkApi REST API.
  class Client < API
    Dir[File.expand_path('../client/*.rb', __FILE__)].each{|f| require f}
    include BallparkApi::Client::Clients
    include BallparkApi::Client::Invoices
  end
end
