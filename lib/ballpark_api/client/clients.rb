module BallparkApi
  class Client
    module Clients

      def clients(params = {})
        name = __method__
        response = get(name.to_s, params)
        if response and response[name]
          response[name].map{|c|client(c.id, params)}
        else
          raise "invalid response"
        end
      end

      def client(id, params = {})
        response = get("clients/#{id}", params)
        if response.id == id
          response
        else
          Hashie::Mash.new
        end
      end

    end
  end
end
