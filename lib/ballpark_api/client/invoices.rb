module BallparkApi
  class Client
    module Invoices

      def invoices(params = {})
        name = __method__
        response = get(name.to_s, params)
        if response and response[name]
          response[name].select{|c|not c.issued_on.blank?}
        else
          raise "invalid response"
        end
      end

      def invoice(id, params = {})
        response = get("invoices/#{id}", params)
        if response.id == id 
          response
        else
          Hashie::Mash.new
        end
      end

    end
  end
end
