require 'faraday'
require 'faraday_middleware'
require 'active_support/all'
require 'ballpark_api/version'

Dir[File.expand_path('../../faraday/*.rb', __FILE__)].each{|f| require f}
require File.expand_path('../ballpark_api/configuration', __FILE__)
require File.expand_path('../ballpark_api/api', __FILE__)
require File.expand_path('../ballpark_api/client', __FILE__)

module BallparkApi

  extend Configuration
  # Alias for BallparkApi::Client.new
  # @return [BallparkApi::Client]
  def self.client(options = {})
    BallparkApi::Client.new(options)
  end

  # Delegate to BallparkApi::Client
  def self.method_missing(method, *args, &block)
    return super unless client.respond_to?(method)
    client.send(method, *args, &block)
  end
end
