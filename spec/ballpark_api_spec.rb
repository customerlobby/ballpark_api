require 'spec_helper'

RSpec.describe BallparkApi do
  after do
    BallparkApi.reset
  end

  describe ".client" do
    it "should be a BallparkApi::Client" do
      expect(BallparkApi.client).to be_a(BallparkApi::Client)
    end
  end

  describe '#api_key' do
    it 'should return the default api key' do
      expect(BallparkApi.api_key).to eq(BallparkApi::Configuration::DEFAULT_API_KEY)
    end
  end

  describe '#api_key=' do
    it 'should set the api key' do
      BallparkApi.api_key = 'test'
      expect(BallparkApi.api_key).to eq('test')
    end
  end

  describe '#api_version' do
    it 'should return the default api version' do
      expect(BallparkApi.api_version).to eq(BallparkApi::Configuration::DEFAULT_API_VERSION)
    end
  end

  describe '#api_version=' do
    it 'should set the api_version' do
      BallparkApi.api_version = '/test/'
      expect(BallparkApi.api_version).to eq('/test/')
    end
  end

  describe '#adapter' do
    it 'should return the default adapter' do
      expect(BallparkApi.adapter).to eq(BallparkApi::Configuration::DEFAULT_ADAPTER)
    end
  end

  describe '#adapter=' do
    it 'should set the adapter' do
      BallparkApi.adapter = :typhoeus
      expect(BallparkApi.adapter).to eq(:typhoeus)
    end
  end

  describe '#api_appid' do
    it 'should return the default api_appid' do
      expect(BallparkApi.api_appid).to eq(BallparkApi::Configuration::DEFAULT_API_APPID)
    end
  end

  describe '#api_appid=' do
    it 'should set the api_appid' do
      BallparkApi.api_appid = 'test'
      expect(BallparkApi.api_appid).to eq('test')
    end
  end

  describe '#configure' do
    BallparkApi::Configuration::VALID_OPTIONS_KEYS.each do |key|

      it "should set the #{key}" do
        BallparkApi.configure do |config|
          config.send("#{key}=", key)
          expect(BallparkApi.send(key)).to eq(key)
        end
      end
    end
  end
end
