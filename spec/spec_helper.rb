require 'ballpark_api'
require 'webmock/rspec'
require 'dotenv'
require 'vcr'

RSpec.configure do |config|
  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    # be_bigger_than(2).and_smaller_than(4).description
    #   # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #   # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  # These two settings work together to allow you to limit a spec run
  # to individual examples or groups you care about by tagging them with
  # `:focus` metadata. When nothing is tagged with `:focus`, all examples
  # get run.
  config.filter_run :focus
  config.run_all_when_everything_filtered = true

  # Turn on color test output.
  config.color = true

  # Limits the available syntax to the non-monkey patched syntax that is recommended.
  # For more details, see:
  #   - http://myronmars.to/n/dev-blog/2012/06/rspecs-new-expectation-syntax
  #   - http://teaisaweso.me/blog/2013/05/27/rspecs-new-message-expectation-syntax/
  #   - http://myronmars.to/n/dev-blog/2014/05/notable-changes-in-rspec-3#new__config_option_to_disable_rspeccore_monkey_patching
  config.disable_monkey_patching!

  # This setting enables warnings. It's recommended, but in some cases may
  # be too noisy due to issues in dependencies.
  config.warnings = true

  # Many RSpec users commonly either run the entire suite or an individual
  # file, and it's useful to allow more verbose output when running an
  # individual spec file.
  if config.files_to_run.one?
    # Use the documentation formatter for detailed output,
    # unless a formatter has already been configured
    # (e.g. via a command-line flag).
    config.default_formatter = 'doc'
  end

  # Print the 10 slowest examples and example groups at the
  # end of the spec run, to help surface which specs are running
  # particularly slow.
  # config.profile_examples = 10

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random

  # Seed global randomization in this process using the `--seed` CLI option.
  # Setting this allows you to use `--seed` to deterministically reproduce
  # test failures related to randomization by passing the same `--seed` value
  # as the one that triggered the failure.
  Kernel.srand config.seed

  Dotenv.load
  env_token = File.expand_path("../../.env.token", __FILE__)
  Dotenv.load(env_token)
  $api_key = ENV['BALLPARK_ACCESS_TOKEN']
  unless $api_key and ENV['BALLPARK_TOKEN_EXPIRES'].to_i - Time.now.to_i  > 30
    VCR.turn_off!
    WebMock.allow_net_connect!
    token = BallparkApi::Client.new.get_token(ENV['BALLPARK_EMAIL'], ENV['BALLPARK_PASSWORD'],
                                              ENV['BALLPARK_CLIENT_ID'], ENV['BALLPARK_CLIENT_SECRET'])
    WebMock.disable_net_connect!
    VCR.turn_on!
    $api_key = token.token
    File.write(env_token, "BALLPARK_ACCESS_TOKEN=#{@api_key}\nBALLPARK_TOKEN_EXPIRES=#{token.expires_at}")
  end
end

def fixture_path
  File.expand_path("../fixtures", __FILE__)
end

def fixture(file)
  File.new(fixture_path + '/' + file)
end

def api_key
  $api_key
end

def api_appid
  ENV['BALLPARK_API_APPID'] || 'customerlobby'
end

VCR.configure do |config|
  config.cassette_library_dir = "#{fixture_path}/vcr_cassettes"
  config.hook_into :webmock
  config.filter_sensitive_data("<API_KEY>") { api_key }
  config.filter_sensitive_data("<API_ENDPONT>") { "http://#{api_appid}.ballparkapp.com" }
end
