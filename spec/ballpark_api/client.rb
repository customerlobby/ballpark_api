require 'spec_helper'

RSpec.describe BallparkApi::Client do

  it 'should connect using the configured endpoint and api version' do
    client = BallparkApi::Client.new
    endpoint = URI.parse("http://#{client.api_appid}.ballparkapp.com#{client.api_version}/")
    connection = client.send(:connection).build_url(nil).to_s
    expect(connection).to eq(endpoint.to_s)
  end

end
