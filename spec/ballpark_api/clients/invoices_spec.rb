require 'spec_helper'

RSpec.describe BallparkApi::Client::Invoices do
  before do
    @client = BallparkApi::Client.new(:api_key => api_key)
    @id = 213330
  end

  describe '#invoices' do
    it 'should return a list of invoices' do
      VCR.use_cassette('invoices') do
        result = @client.invoices
        expect(result).to be_a(Array)
        expect(result.size).to eq(5)
      end
    end
    it 'should return a list with id > @id' do
      VCR.use_cassette('invoices-after_id') do
        result = @client.invoices({:after_id => @id})
        expect(result.all?{|c|c[:id] > @id}).to be true
      end
    end
    it 'should return a list with id < @id' do
      VCR.use_cassette('invoices-before_id') do
        result = @client.invoices({:before_id => @id})
        expect(result.all?{|c|c[:id] < @id}).to be true
      end
    end
  end

  describe '#invoice' do
    context 'found' do
      before do
        VCR.use_cassette('invoice') do
          @invoice = @client.invoice(@id)
        end
      end
      it 'should be a hash with the same id' do
        expect(@invoice).to be_a(Hash)
        expect(@invoice.id).to eq(@id)
      end
    end
    it 'should return an empty invoice if not found' do
      VCR.use_cassette('invoice-not-found') do
        result = @client.invoice(0)
        expect(result).to be_a(Hash)
        expect(result.id).to be_nil
      end
    end
  end

end
