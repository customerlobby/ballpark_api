require 'spec_helper'

RSpec.describe BallparkApi::Client::Clients do
  before do
    @client = BallparkApi::Client.new(:api_key => api_key)
    @client.api_appid = api_appid unless api_appid.empty?
    @id = 104819
  end

  describe '#clients' do
    context 'list of clients' do
      before do
        VCR.use_cassette('clients') do
          @clients = @client.clients
        end
      end
      it 'should contain items' do
        expect(@clients).to be_a(Array)
        expect(@clients.size).to eq(50)
      end
    end
    it 'should return a list with id > @id' do
      VCR.use_cassette('clients-after_id') do
        result = @client.clients({:after_id => @id})
        expect(result.all?{|c|c[:id] > @id}).to be true
      end
    end
    it 'should return a list with id < @id' do
      VCR.use_cassette('clients-before_id') do
        result = @client.clients({:before_id => @id})
        expect(result.all?{|c|c[:id] < @id}).to be true
      end
    end
  end

  describe '#client' do
    context 'found' do
      before do
        VCR.use_cassette('client') do
          @c = @client.client(@id)
        end
      end
      it 'should be hash with the same id' do
        expect(@c).to be_a(Hash)
        expect(@c.id).to eq(@id)
      end
    end
    context 'not found' do 
      it 'should return an empty customer' do
        VCR.use_cassette('client-not-found') do
          result = @client.client(0)
          expect(result).to be_a(Hash)
          expect(result.id).to be_nil
        end
      end
    end
  end

end
